package com.ozdemo.ozrpc.rmi.server.service;

import com.ozdemo.ozrpc.rmi.server.pojo.User;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @Description: hello服务接口
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/10/14 15:06
 */
public interface HelloService extends Remote {
    public String sayHello(User user) throws RemoteException;
}
