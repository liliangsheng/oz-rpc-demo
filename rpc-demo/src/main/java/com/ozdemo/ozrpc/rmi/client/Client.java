package com.ozdemo.ozrpc.rmi.client;

import com.ozdemo.ozrpc.rmi.server.pojo.User;
import com.ozdemo.ozrpc.rmi.server.service.HelloService;

import java.rmi.Naming;

/**
 * @Description: 客户端程序
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/10/14 15:18
 */
public class Client {
    public static void main(String[] args) {
        try {
            //在RMI服务注册表中查找名称为RHello的对象，并调用其上的方法
            HelloService helloService = (HelloService) Naming.lookup("//127.0.0.1:6400/zm");//获取远程对象
            User user = new User();
            user.setName("james");
            System.out.println(helloService.sayHello(user));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
