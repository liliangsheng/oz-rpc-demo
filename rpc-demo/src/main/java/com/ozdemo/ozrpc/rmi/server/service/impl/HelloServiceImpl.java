package com.ozdemo.ozrpc.rmi.server.service.impl;

import com.ozdemo.ozrpc.rmi.server.pojo.User;
import com.ozdemo.ozrpc.rmi.server.service.HelloService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * @Description: HelloService实现
 * 远程服务对象实现类写在服务端；必须继承UnicastRemoteObject或其子类
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/10/14 15:10
 */
public class HelloServiceImpl extends UnicastRemoteObject implements HelloService {

    private static final long serialVersionUID = -7861497176153334891L;

    public HelloServiceImpl() throws RemoteException {
        super();
    }

    /**
     * 因为UnicastRemoteObject的构造方法抛出了RemoteException异常，因此这里默认的构造方法必须写，必须
     * 声明抛出RemoteException异常
     *
     * @throws
     */
    public String sayHello(User user) throws RemoteException {
        System.out.println("this is server, hello:" + user.getName());
        return "success";
    }
}
