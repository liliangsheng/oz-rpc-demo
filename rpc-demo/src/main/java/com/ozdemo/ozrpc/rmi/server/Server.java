package com.ozdemo.ozrpc.rmi.server;

import com.ozdemo.ozrpc.rmi.server.service.impl.HelloServiceImpl;
import com.ozdemo.ozrpc.rmi.server.service.HelloService;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

/**
 * @Description: 服务器端
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/10/14 15:14
 */
public class Server {
    public static void main(String[] args) {
        try {
            HelloService helloService = new HelloServiceImpl(); // 创建一个远程对象，同时也会创建stub对象、skeleton对象
            // 本地主机上的远程对象注册表Registry的实例，并指定端口为8888，这一步必不可少（Java默认端口是1099），必不可缺的一步，缺少注册表创建，则无法绑定对象到远程注册表上
            LocateRegistry.createRegistry(6400); //启动注册服务
            try {
                //绑定的URL标准格式为：rmi://host:port/name(其中协议名可以省略，下面两种写法都是正确的）客户端程序启动服务端程序客户端调用服务端：
                Naming.bind("//127.0.0.1:6400/zm", helloService); //将stub引用绑定到服务地址上
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("service bind already!!");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
