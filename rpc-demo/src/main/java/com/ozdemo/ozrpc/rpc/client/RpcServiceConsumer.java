package com.ozdemo.ozrpc.rpc.client;

import com.ozdemo.ozrpc.rpc.handler.ClientRpcHandler;
import com.ozdemo.pojo.RpcRequest;
import com.ozdemo.pojo.RpcResponse;
import com.ozdemo.serializer.JsonSerializer;
import com.ozdemo.serializer.RpcDecoder;
import com.ozdemo.serializer.RpcEncoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Description: 消费者
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/10/17 16:45
 */
public class RpcServiceConsumer {

    /**
     * 1.创建一个线程池对象  -- 它要处理我们自定义事件
     */
    private static ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    /**
     * 2.声明一个自定义事件处理器  ClientRpcHandler
     */
    private static ClientRpcHandler userClientHandler;


    /**
     * 3.编写方法,初始化客户端  ( 创建连接池  bootStrap  设置bootstrap  连接服务器)
     *
     * @param ip
     * @param port
     * @throws InterruptedException
     */
    private static void initClient(String ip, int port) throws InterruptedException {
        //1) 初始化UserClientHandler
        userClientHandler = new ClientRpcHandler();
        //2)创建连接池对象
        EventLoopGroup group = new NioEventLoopGroup();
        //3)创建客户端的引导对象
        Bootstrap bootstrap = new Bootstrap();
        //4)配置启动引导对象
        bootstrap.group(group)
                //设置通道为NIO
                .channel(NioSocketChannel.class)
                //设置请求协议为TCP
                .option(ChannelOption.TCP_NODELAY, true)
                //监听channel 并初始化
                .handler(new ChannelInitializer<SocketChannel>() {
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        //获取ChannelPipeline
                        ChannelPipeline pipeline = socketChannel.pipeline();
                        //设置编码
                        pipeline.addLast(new RpcEncoder(RpcRequest.class, new JsonSerializer()));
                        pipeline.addLast(new RpcDecoder(RpcResponse.class, new JsonSerializer()));
                        //添加自定义事件处理器
                        pipeline.addLast(userClientHandler);
                    }
                });

        //5)连接服务端
        bootstrap.connect(ip, port).sync();
    }

    /**
     * 4.编写一个方法,使用JDK的动态代理创建对象
     * serviceClass 接口类型
     *
     * @param serviceClass
     * @return
     */
    public static Object createProxy(Class<?> serviceClass) {
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                new Class[]{serviceClass}, new InvocationHandler() {
                    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
                        //1)初始化客户端client
                        if (userClientHandler == null) {
                            initClient("127.0.0.1", 6400);
                        }

                        //2)给UserClientHandler 设置param参数
                        RpcRequest rpcRequest = new RpcRequest();
                        rpcRequest.setClassName(serviceClass.getSimpleName());
                        rpcRequest.setMethodName(method.getName());
                        rpcRequest.setParameterTypes(method.getParameterTypes());
                        rpcRequest.setParameters(objects);
                        rpcRequest.setRequestId(UUID.randomUUID().toString());
                        userClientHandler.setRpcRequest(rpcRequest);

                        //3).使用线程池,开启一个线程处理处理call() 写操作,并返回结果
                        RpcResponse rpcResponse = (RpcResponse) executorService.submit(userClientHandler).get();

                        //4)return 结果
                        return rpcResponse.getReturnData();
                    }
                });
    }

}
