package com.ozdemo.ozrpc.rpc;

import com.ozdemo.ozrpc.rpc.client.RpcServiceConsumer;
import com.ozdemo.service.IUserService;

/**
 * @Description: 消费者启动类
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/10/17 16:45
 */
public class ConsumerBoot {

    public static void main(String[] args) throws InterruptedException {
        //1.创建代理对象
        IUserService userService = (IUserService) RpcServiceConsumer.createProxy(IUserService.class);

        //2.循环给服务器写数据
        while (true){
            String result = userService.testUser("Are you ok !!");
            System.out.println(result);
            Thread.sleep(1000);
        }

    }
}
