package com.ozdemo.serializer;

import java.io.IOException;

/**
 * @Description: 序列化接口
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/10/17 17:12
 */
public interface Serializer {
    /**
     * java对象转换为二进制
     *
     * @param object
     * @return
     */

    byte[] serialize(Object object) throws IOException;


    /**
     * 二进制转换成java对象
     *
     * @param clazz
     * @param bytes
     * @param <T>
     * @return
     */

    <T> T deserialize(Class<T> clazz, byte[] bytes) throws IOException;
}
