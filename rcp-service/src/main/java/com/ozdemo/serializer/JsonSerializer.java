package com.ozdemo.serializer;

import com.alibaba.fastjson.JSON;

/**
 * @Description: Json序列化实现
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/10/17 17:13
 */
public class JsonSerializer implements Serializer {
    @Override
    public byte[] serialize(Object object) {
        return JSON.toJSONBytes(object);
    }

    @Override
    public <T> T deserialize(Class<T> clazz, byte[] bytes) {
        return JSON.parseObject(bytes, clazz);
    }
}
