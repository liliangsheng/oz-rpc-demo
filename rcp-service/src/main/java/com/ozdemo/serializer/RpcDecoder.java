package com.ozdemo.serializer;

import com.ozdemo.pojo.RpcRequest;
import com.ozdemo.pojo.RpcResponse;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @Description: rpc解码
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/10/17 17:16
 */
public class RpcDecoder extends ByteToMessageDecoder {
    private Class<?> clazz;
    private Serializer serializer;

    public RpcDecoder(Class<?> clazz, Serializer serializer) {
        this.clazz = clazz;
        this.serializer = serializer;
    }

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        // 消息的长度
        int length = byteBuf.readInt();
        if(length == 0){
            return;
        }
        //读取数据
        byte[] data = new byte[length];
        byteBuf.readBytes(data);

        if (clazz != null) {
            if(clazz == RpcRequest.class){
                RpcRequest rpcRequest = (RpcRequest) serializer.deserialize(clazz, data);
                list.add(rpcRequest);
            }else if(clazz == RpcResponse.class){
                RpcResponse rpcResponse = (RpcResponse) serializer.deserialize(clazz, data);
                list.add(rpcResponse);
            }
        }
        //回收已读字节
        byteBuf.discardReadBytes();
    }
}
