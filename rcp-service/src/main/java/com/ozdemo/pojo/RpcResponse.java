package com.ozdemo.pojo;

import java.io.Serializable;

/**
 * @Description: rpc响应
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/10/17 17:09
 */
public class RpcResponse implements Serializable {

    private static final long serialVersionUID = 2107708698647332656L;
    /**
     * 响应对象的ID
     */
    private String responseId;

    /**
     * 方法返回类型
     */
    private Class<?> returnType;

    /**
     * 返回数据
     */
    private Object returnData;


    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public Class<?> getReturnType() {
        return returnType;
    }

    public void setReturnType(Class<?> returnType) {
        this.returnType = returnType;
    }

    public Object getReturnData() {
        return returnData;
    }

    public void setReturnData(Object returnData) {
        this.returnData = returnData;
    }
}
