package com.ozdemo.provider;

import com.ozdemo.handler.ServerRpcHandler;
import com.ozdemo.pojo.RpcRequest;
import com.ozdemo.pojo.RpcResponse;
import com.ozdemo.serializer.JsonSerializer;
import com.ozdemo.serializer.RpcDecoder;
import com.ozdemo.serializer.RpcEncoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * @Description: 服务提供者 启动类
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/10/17 16:54
 */
public class RpcServiceProvider {

    /**
     * 提供者服务启动
     *
     * @param port
     * @throws InterruptedException
     */
    public static void startServer(int port) throws InterruptedException {
        //1.创建两个线程池对象
        NioEventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup workGroup = new NioEventLoopGroup();

        //2.创建服务端的启动引导对象
        ServerBootstrap serverBootstrap = new ServerBootstrap();

        //3.配置启动引导对象
        serverBootstrap.group(bossGroup, workGroup)
                //设置通道为NIO
                .channel(NioServerSocketChannel.class)
                //创建监听channel
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    protected void initChannel(NioSocketChannel nioSocketChannel) throws Exception {
                        //获取管道对象
                        ChannelPipeline pipeline = nioSocketChannel.pipeline();
                        //给管道对象pipeLine 设置编码
                        pipeline.addLast(new RpcEncoder(RpcResponse.class, new JsonSerializer()));
                        pipeline.addLast(new RpcDecoder(RpcRequest.class, new JsonSerializer()));
                        //把我们自顶一个ChannelHandler添加到通道中
                        pipeline.addLast(new ServerRpcHandler());
                    }
                });

        //4.绑定端口
        serverBootstrap.bind(port).sync();
    }
}
