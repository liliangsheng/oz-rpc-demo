package com.ozdemo;

import com.ozdemo.provider.RpcServiceProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description: 启动类
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/10/17 16:45
 */
@SpringBootApplication
public class ServerBoot {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(ServerBoot.class, args);
        //启动服务器
        RpcServiceProvider.startServer(6400);
    }
}
