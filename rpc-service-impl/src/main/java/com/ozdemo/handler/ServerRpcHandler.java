package com.ozdemo.handler;

import com.ozdemo.pojo.RpcRequest;
import com.ozdemo.pojo.RpcResponse;
import com.ozdemo.utils.SpringContextUtils;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.lang.reflect.Method;

/**
 * @Description: 服务端自定义处理类
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/10/17 16:45
 */
public class ServerRpcHandler extends ChannelInboundHandlerAdapter {

    //当客户端读取数据时,该方法会被调用
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        RpcRequest rpcRequest = (RpcRequest) msg;
        if (rpcRequest != null) {
            Object object = SpringContextUtils.getBean(rpcRequest.getClassName());
            Class<?> clzz = object.getClass();
            if (clzz != null) {
                Method method = clzz.getMethod(rpcRequest.getMethodName(), rpcRequest.getParameterTypes());
                Object result = method.invoke(object, rpcRequest.getParameters());
                RpcResponse rpcResponse = new RpcResponse();
                rpcResponse.setReturnType(method.getReturnType());
                rpcResponse.setReturnData(result);
                rpcResponse.setResponseId(rpcRequest.getRequestId());
                ctx.writeAndFlush(rpcResponse);
            }
        }
    }
}
