package com.ozdemo.impl;

import com.ozdemo.service.IUserService;
import org.springframework.stereotype.Service;

/**
 * @Description: 用户服务实现
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/10/17 16:46
 */
@Service("IUserService")
public class UserServiceImpl implements IUserService {

    @Override
    public String testUser(String msg) {
        return "UserService is success!msg:" + msg;
    }
}
